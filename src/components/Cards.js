import React from 'react';
import './Cards.css';
import CardItem from './CardItem';
import 'react-bootstrap';

function Cards() {
  return (



    <div className='cards'>

       <h1 className='header_aboutme'>About Me</h1>
      
      <div className='cards__container'>
        


        <div className='cards__wrapper'>
        
        
        <ul className='cards__items'>
            <CardItem
              src='images/rey.jpg'
              text='Good day! I am Rey Jesus Teves. This website was developed using React.js and hosted in Vercel. I am training as a Full Stack Software Engineer. You can contact me through the links on this page.'
              label='About Me'
              // path='/about'


            />

          <CardItem
              src='images/skills.JPG'
              text='I can design websites using React.js, Javascript, HTML, CSS and Bootstrap. I know how to use Git Command Line Interface and Node Package Manager. I can also create databases using PHP, MySQL, MongoDB Atlas, Express, Node.js. I can host websites using Vercel, Heroku, Cpanel and Gitlab Pages.'
              label='Skills'
              // path='/about'
            />
          
         </ul> 

         <h1 className='header_space'>My Projects</h1>
          
          <ul className='cards__items'>
            <CardItem
              src='images/project-1.jpg'
              text='This website was developed using HTML, CSS, Bootstrap and Javascript'
              label='Portfolio (Material Design)'
              path='/project-1'
            />
          

             
            <CardItem
              src='images/project-2.JPG'
              text='This website was developed using MongoDB Atlas, Express, Node.js, Heroku and Gitlab Pages'
              label='A Design For An Online Learning Website'
              path='/project-2'
            />

           
            
           <CardItem
              src='images/project-3.JPG'
              text='A UI (User Interface) that can switch data from table to card view, to make the contents more appealing.'
              label='Table to Card Viewer'
              path='/project-3'
            />
           </ul>



          <h1 className='header_space'>Contact</h1>
          
          <ul className='cards__items'>

            <CardItem
              src='images/email.jpg'
              text='Email me using your email app.'
              label='Email'
              path='/email'
            />

            <CardItem
              src='images/linkedin.png'
              text='View my Linkedin Page.'
              label='Linkedin Page'
              path='/linkedin'
            />


            <CardItem
              src='images/contactpage.JPG'
              text='Open my contact page.'
              label='Contact Page'
              path='/contactpage'
            />
          
          </ul>
          


        </div>
      </div>
    </div>
  );
}

export default Cards;
