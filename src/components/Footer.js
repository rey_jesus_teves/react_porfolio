import React from 'react';
import './Footer.css';

function Footer() {
return (

<footer>
    <section className='footer-container'>
        <span className='footer-text'>Rey Jesus Teves - ReyWebPages&copy; 2020</span>
    </section>
</footer>

);

}
export default Footer;